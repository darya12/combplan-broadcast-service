package com.orioninc.combplanbroadcastservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CombplanBroadcastServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CombplanBroadcastServiceApplication.class, args);
	}

}
